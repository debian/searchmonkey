searchmonkey (0.8.3-1.2) unstable; urgency=medium

  [ Andreas Rönnquist ]
  * Non-maintainer upload.
  * Add patch to fix implicit declarations (Closes: #1066468)

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 10.
  * Set upstream metadata fields: Archive, Bug-Database, Name (from
    ./configure).

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 21 Apr 2024 22:08:58 +0200

searchmonkey (0.8.3-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control: Fix Vcs-* fields. (Closes: #1010243)
  * debian/searchmonkey.desktop: Fix broken icon location. (Closes: #901934)
  * debian/rules: Fix broken pixmap symlink. (Closes: #910424)

 -- Boyuan Yang <byang@debian.org>  Tue, 26 Apr 2022 21:46:44 -0400

searchmonkey (0.8.3-1) unstable; urgency=medium

  * New upstream release
  * d/control: Add Build-Depends intltool, libzip-dev, libpoppler-glib-dev
  * Update Vcs fields to new debian git repository
  * Remove patches that are merged upstream
  * Update debian/compat to 9

 -- Varun Hiremath <varun@debian.org>  Fri, 23 Mar 2018 13:32:19 -0400

searchmonkey (0.8.1-9) unstable; urgency=low

  [ Benjamin Kerensa ]
  * Add self as co-maintainer

  [ Varun Hiremath ]
  * Fix a typo in the desktop file (Closes: #663661)
  * Add a patch fix-crash.diff to fix crashes, thanks to Peter
    <peter@pblackman.plus.com> (Closes: #625643)
  * Update build.diff patch to fix broken link (Closes: #705849)
  * Update Standards-Version to 3.9.4

 -- Varun Hiremath <varun@debian.org>  Sun, 11 Aug 2013 20:06:47 -0400

searchmonkey (0.8.1-8) unstable; urgency=low

  * Add patches/fix-format-security.diff, thanks to Peter Green
    <plugwash@p10link.net>, fixes FTBFS (Closes: #646490)
  * Bump Standards-Version to 3.9.2
  * Add searchmonkey menu file (Closes: #587793)

 -- Varun Hiremath <varun@debian.org>  Sun, 13 Nov 2011 00:40:42 -0500

searchmonkey (0.8.1-7) unstable; urgency=low

  * Switch to source format 3.0
  * Clean up debian/rules and the build procedure (Closes: #577359)
  * debian/control:
    + Remove Torsten Werner from Uploaders, as requested
    + Bump Standards-Version to 3.8.4
  * Fix debian/copyright

 -- Varun Hiremath <varun@debian.org>  Sun, 11 Apr 2010 20:37:16 -0400

searchmonkey (0.8.1-6) unstable; urgency=low

  * debian/control:
    + Add Homepage header and replace XS-Vcs with Vcs
    + Bump up Standards-Version to 3.7.3
    + Build-Depend on debhelper >= 6
  * debian/compat: Bump to 6
  * Remove encoding field from desktop file

 -- Varun Hiremath <varun@debian.org>  Sat, 19 Jan 2008 12:27:23 +0530

searchmonkey (0.8.1-5) unstable; urgency=low

  * Add searchmonkey.desktop (Closes: #442178)
  * debian/rules: implement get-orig-source

 -- Varun Hiremath <varunhiremath@gmail.com>  Fri, 14 Sep 2007 11:18:18 +0530

searchmonkey (0.8.1-4) unstable; urgency=low

  * Remove config.guess and config.sub in clean target. (Closes: #424265)

 -- Torsten Werner <twerner@debian.org>  Wed, 16 May 2007 19:24:03 +0200

searchmonkey (0.8.1-3) unstable; urgency=low

  * Clean up debian/rules.

 -- Torsten Werner <twerner@debian.org>  Wed,  7 Mar 2007 07:28:49 +0100

searchmonkey (0.8.1-2) unstable; urgency=low

  * Fix a broken symlink in the package. Closes: #413667

 -- Torsten Werner <twerner@debian.org>  Tue,  6 Mar 2007 21:09:19 +0100

searchmonkey (0.8.1-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varunhiremath@gmail.com>  Mon, 12 Feb 2007 21:06:49 +0530

searchmonkey (0.7.3-1) experimental; urgency=low

  * New upstream release

 -- Varun Hiremath <varunhiremath@gmail.com>  Fri,  1 Dec 2006 05:28:27 +0530

searchmonkey (0.7.2-1) experimental; urgency=low

  [ Varun Hiremath ]
  * New upstream release
  * Update debian/watch file

  [ Torsten Werner ]
  * Uploading to experimental because Debian is in soft freeze.

 -- Torsten Werner <twerner@debian.org>  Sun, 19 Nov 2006 10:57:28 +0100

searchmonkey (0.7.1-1) unstable; urgency=low

  * New upstream release
  * Add XS-X-Vcs-Svn header in debian/control
  * Add debian/watch file

 -- Varun Hiremath <varunhiremath@gmail.com>  Fri,  3 Nov 2006 18:20:38 +0530

searchmonkey (0.6.3-1) unstable; urgency=low

  * New upstream release.

 -- Varun Hiremath <varunhiremath@gmail.com>  Sun,  8 Oct 2006 23:51:37 +0530

searchmonkey (0.6.2-1) unstable; urgency=low

  * New upstream release.
  * Add myself to Uploaders: in debian/control.

 -- Torsten Werner <twerner@debian.org>  Fri,  6 Oct 2006 15:43:25 +0200

searchmonkey (0.6.1-1) unstable; urgency=low

  * Initial release (Closes: #386960)

 -- Varun Hiremath <varunhiremath@gmail.com>  Tue, 12 Sep 2006 23:37:57 +0530
